const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const userRoutes = require('./routes/userRoutes');
const errorHandler = require('./middleware/errorMiddleware');
const specs = require('./swagger');
const swaggerUi = require('swagger-ui-express')
const app = express();

app.use(bodyParser.json());
app.use('/api', userRoutes);
app.use(errorHandler);

// Initialize Passport and configure JWT strategy for authentication
app.use(passport.initialize());
require('./config/passport')(passport);

// Serve Swagger documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});