const User = require('../models/User');
const asyncHandler = require('express-async-handler');
const bcrypt = require('bcrypt');

// Sign up a new user
exports.signUp = asyncHandler(async(req, res) => {
    const { name, email, phone, password } = req.body;

    // Check if the email or phone number is already in use
    const existingUser = await User.findOne({
        where: {
            [Op.or]: [{ email }, { phone }],
        },
    });

    if (existingUser) {
        return res.status(400).json({ error: 'User already exists' });
    }

    // Hash the password before saving it to the database
    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await User.create({ name, email, phone, password: hashedPassword });
    res.status(201).json(user);
});

// Login a user
exports.login = asyncHandler(async(req, res) => {
    const { phone, password } = req.body;

    const user = await User.findOne({
        where: { phone },
    });

    if (!user) {
        return res.status(401).json({ error: 'User not found' });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
        return res.status(401).json({ error: 'Incorrect password' });
    }

    // User is authenticated, you can generate a token and handle authentication here

    res.status(200).json({ message: 'Login successful' });
});

// Get user profile
exports.getUserProfile = asyncHandler(async(req, res) => {
    const user = req.user; // Passport stores the authenticated user in req.user
    res.json(user);
});