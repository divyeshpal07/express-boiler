// src/routes/userRoutes.js
const express = require('express');
const passport = require('passport');
const router = express.Router();
const UserController = require('../controllers/UserController');

router.post('/signup', UserController.signUp);
router.post('/login', UserController.login);

/**
 * @swagger
 * /users/profile:
 *   get:
 *     summary: Get user profile
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       200:
 *         description: Successfully retrieved user profile
 *       401:
 *         description: Unauthorized - Token is missing or invalid
 */
router.get('/profile', passport.authenticate('jwt', { session: false }), UserController.getUserProfile);



module.exports = router;