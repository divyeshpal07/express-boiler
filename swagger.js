// swagger.js
const swaggerJsdoc = require('swagger-jsdoc');
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'User API',
            version: '1.0.0',
            description: 'User management API',
        },
        servers: [{
            url: 'http://localhost:3000/api', // Adjust the URL accordingly
        }, ],
    },
    apis: ['./node-express/routes/userRoutes.js'],
};
const specs = swaggerJsdoc(options);
module.exports = specs;